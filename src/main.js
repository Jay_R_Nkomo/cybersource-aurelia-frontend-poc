import environment from '../config/environment.json';
import {PLATFORM} from 'aurelia-pal';
import {HttpClient} from 'aurelia-http-client';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    //.plugin('aurelia-dialog')
    .plugin('aurelia-http-client')
    .feature(PLATFORM.moduleName('resources/index'));

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
