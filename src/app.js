import {HttpClient} from 'aurelia-http-client';

export class App {
  message = 'Hello World!';

  initiateCheckout() {
   //Step 1: Get Capture Context which is used by FlexAPI
   console.log("I was clicked!");

   httpClient.get('package.json')
   .then(data => {
     console.log(data.description)
   });
 }

}
